$:.unshift(__FILE__, ".")

require 'app'

Encoding.default_external = Encoding::UTF_8

use Rack::ShowExceptions

run App.new
