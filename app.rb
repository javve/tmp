# encoding: utf-8

require 'bundler/setup'
require 'sinatra'
require 'i18n'
require 'i18n/backend/fallbacks'
require 'sinatra/asset_pipeline'
#require 'mnd'

class App < Sinatra::Base

  asset_paths = %w(assets, assets2)
  #asset_paths << "#{Gem.loaded_specs['mnd'].full_gem_path}/assets"
  #puts Gem.loaded_specs['mnd'].full_gem_path
  set :assets_prefix, asset_paths

  register Sinatra::AssetPipeline

  # before '/:locale/*' do
  #   I18n.locale       =       params[:locale]
  #   request.path_info = '/' + params[:splat ][0]
  #   set_default_page_title
  # end

  # configure do
  #   I18n::Backend::Simple.send(:include, I18n::Backend::Fallbacks)
  #   I18n.load_path += Dir[File.join(settings.root, 'locales', '*.yml')]
  #   I18n.backend.load_translations
  # end

  get '/' do
    erb :index
  end

  # private
  # def set_default_page_title
  #   @page_title_common = " - Mynewsdesk PR Insights"
  #   @page_title = "Mynewsdesk PR Insights"
  # end

  # def set_locale
  #   I18n.locale = params[:locale]
  # end

  # helpers do
  #   def find_template(views, name, engine, &block)
  #     I18n.fallbacks[I18n.locale].each do |locale|
  #       if File.exists? File.join('views', "#{name}.#{locale}.html.erb")
  #         #raise 'step1'
  #         super(views, "#{name}.#{locale}.html", engine, &block)
  #       elsif File.exists? File.join('views', "#{name}.html.erb")
  #         #raise 'step2'
  #         super(views, "#{name}.html", engine, &block)
  #       else
  #         #raise 'step3'
  #         super(views, name, engine, &block)
  #       end
  #     end
  #   end

  #   def link_to *args
  #     "<a href='#{args[1]}'>#{args[0]}</a>"
  #   end

  #   # def method_missing *args
  #   # end
  # end
end
